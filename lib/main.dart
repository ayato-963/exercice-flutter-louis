// ignore_for_file: prefer_const_constructors, unnecessary_new, prefer_const_literals_to_create_immutables
import 'package:flutter/material.dart';
import 'equipementCard.dart';
import 'equipement.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(
        equipementList: <Equipement>[
          Equipement(name: "azerty1", nds: "1"),
          Equipement(name: "azerty2", nds: "2"),
          Equipement(name: "azerty3", nds: "3"),
          Equipement(name: "azerty4", nds: "4"),
          Equipement(name: "azerty5", nds: "5"),
          Equipement(name: "azerty6", nds: "6"),
          Equipement(name: "azerty7", nds: "7"),
          Equipement(name: "azerty8", nds: "8"),
        ],
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final List<Equipement> equipementList;
  const MyHomePage({Key? key, required this.equipementList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Container(
                margin: new EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 50),
                    Text(
                      "Xefi Lyon",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Text(
                      "2507 Avenue de l'Europe\n69140 Rillieux-la-Pape",
                      style: TextStyle(color: Colors.grey, height: 1.5),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        "04 72 83 75 75",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        "xefi@gmail.com",
                        style: TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 15),
                      child: Text(
                        "Lorem ipsum dolor sit amet, Integer cursus, mi vel dictum rutrum, ex mauris pharetra enim, vel suscipit turpis dui vel urna. Aliquam fermentum lacinia blandit.",
                        style: TextStyle(color: Colors.grey, height: 2),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 25, bottom: 25),
                      child: Text(
                        "Equipements (" +
                            equipementList.length.toString() +
                            ")",
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                )),
            Expanded(
              child: ListView(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                children: [
                  ...List.generate(
                    equipementList.length,
                    (index) => EquipementCard(
                        nds: equipementList[index].nds,
                        name: equipementList[index].name),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
